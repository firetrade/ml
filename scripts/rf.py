import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

DATA_TRAIN_PATH = '../input/train.csv'
DATA_TEST_PATH = '../input/test.csv'



train = pd.read_csv(DATA_TRAIN_PATH, dtype={'id': np.int32})
# test = pd.read_csv(DATA_TEST_PATH, dtype={'id': np.int32})

features = train.columns
cats = [feat for feat in features if 'cat' in feat]

for feat in cats:
    train[feat],idx =  train[feat].factorize()
    # test[feat] = idx.get_indexer(test[feat])


train_y=train['loss']
train_x = train.drop(['id', 'loss'], axis=1)


def estimate(x):
    X_train, X_test, y_train, y_test = train_test_split(train_x, train_y,
                                                        train_size=x,
                                                        test_size=1000)
    regr_rf = RandomForestRegressor(max_depth=20, n_estimators=15,n_jobs=4)
    regr_rf.fit(X_train, y_train)
    y_pred = regr_rf.predict(X_test)
    return mean_absolute_error(y_test,y_pred)


param_steps=np.arange(1000, 30000, 1000)

estimate_vectorized = np.vectorize(lambda x : estimate(x))

errors=estimate_vectorized(param_steps)

#plot errors depending on number of samples for train
plt.plot(param_steps, errors)

plt.show()
